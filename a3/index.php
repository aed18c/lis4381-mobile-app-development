<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Allyson Davis">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Frequently, not only will you be asked to design and develop Web applications, but you will also be asked to create (design) database solutions that interact with the Web application—and, in fact, the data repository is the *core* of all Web applications. Hence, the following business requirements.<br><br>A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:<br><br>	1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.<br>		2. A store has many pets, but each pet is sold by only one store.<br><br>
				
					<strong>Solutions:</strong>
					<ul style="text-align: left;">
						<li><a href="docs/a3.mwb">A3 MWB</a></li>
						<li><a href="docs/a3.sql">A3 SQL</a></li>
					</ul>
				</p>

				<h4>Petstore ERD</h4>
				<img src="img/erd.PNG" class="img-responsive center-block" alt="petstore erd">

				<h4>My Event App: Actvity 1</h4>
				<img src="img/screen1.PNG" class="img-responsive center-block" alt="my event app screen 1">

				<h4>My Event App: Actvity 2</h4>
				<img src="img/screen2.PNG" class="img-responsive center-block" alt="my event app screen 2">

				<h4>My Event App: Actvity 3</h4>
				<img src="img/screen3.PNG" class="img-responsive center-block" alt="my event app screen 3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
