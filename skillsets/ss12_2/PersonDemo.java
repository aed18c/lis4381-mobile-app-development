//Allyson Davis
//ss12 - PersonDemo

import java.util.Scanner;

public class PersonDemo {
    public static void main(String args[]) {
        Person.getRequirements();

        Person person1 = new Person();
        Scanner scnr = new Scanner(System.in);

        System.out.println("/////Below are default constructor values://///\n");
        System.out.println("Inside person default constructor.\n");
        System.out.println("Fname = " + person1.getFname());
        System.out.println("Lname = " + person1.getLname());
        System.out.println("Age = " + person1.getAge());

        System.out.println("\n/////Below are user-entered values://///\n");
        System.out.print("Fname: ");
        String fname = scnr.nextLine();
        System.out.print("Lname: ");
        String lname = scnr.nextLine();
        System.out.print("Age: ");
        int age = scnr.nextInt();

        Person person2 = new Person(fname, lname, age);
        
        System.out.println("\nInside person constructor with parameters.\n");
        System.out.println("Fname = " + person2.getFname());
        System.out.println("Lname = " + person2.getLname());
        System.out.println("Age = " + person2.getAge());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values/////\n");
        person1.setFname("Bob");
        person1.setLname("Wilson");
        person1.setAge(42);
        person1.print();
    }//end main

}//end class