//Allyson Davis
//ss12 - Person 

public class Person {
    private String fname;
    private String lname;
    private int age;

    //requirements
    public static void getRequirements(){
        System.out.println("Developer: Allyson Davis");
        System.out.println("Set up default and user-entered data constructors.\n");
    }

    //default constructor
    Person() {
        this.fname = "John";
        this.lname = "Doe";
        this.age = 21;
    }//end default constructor

    Person(String fname, String lname, int age) {
        this.fname = fname;
        this.lname = lname;
        this.age = age;
    }//end second constructor

    public void setFname(String fname) {
        this.fname = fname;
    }//end setFname

    public void setLname(String lname) {
        this.lname = lname;
    }//end setLname

    public void setAge(int age) {
        this.age = age;
    }//end setAge

    public String getLname() {
        return lname;
    }//end getLname

    public String getFname() {
        return fname;
    }//end getFname

    public int getAge() {
        return age;
    }//end getAge

    public void print() {
        System.out.println("Fname: " + this.getFname() + ", Lname: " + this.getLname() + ", Age: " + this.getAge());
    }
}//end class