> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Allyson Davis
### Project # 2 Requirements:

1. Add server-side validation and regular expressions
2. Add edit_petstore.php and edit_petstore_process.php
3. Add delete_petstore.php
4. Create a simple rss feed


#### README.md file should include the following items:

* Screenshot of index.php showing database contents
* Screenshot of edit_petstore.php
* Screenshot of server-side validation error
* Screenshot of delete_petstore.php message
* Screenshot of basic rss feed page


> 
> 
> 

#### Assignment Screenshots:

*index.php*:

![P2 index Screenshot](img/index.PNG)

| *edit_petstore.php*:      | *Sedit_petstore_process.php (includes error.php)*:    |
| :------------- | -----------: |
|  ![edit_petstore.php](img/edit.PNG) | ![edit process error](img/edit-error.PNG)    |

*delete petstore message*:

![delete petstore message](img/delete.PNG)

*Carousel (Home page)*:

![carousel screenshot](img/portfolio.gif)


*RSS Feed - Science Daily*:

![rss feed screenshot](img/rss.PNG)


#### Assignment Links:

*Local LIS4381 web app:*
[local lis4381 link](http://localhost/repos/lis4381/)

