> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 : Mobile Web App Development

## Allyson Davis
### Assignment #2 Requirements:

1. Create Healthy Recipes Android app
2. Provide screenshots of completed app
3. Provide three screenshots of completed Java skill sets

#### README.md file should include the following items:

* Screenshots of completed Healthy Recipes app
* Screenshots of completed Java skill sets (1, 2, & 3)

> 
> 
> 
>



#### Assignment Screenshots:

| *Screenshot of App - Homepage*:	| *Screenshot of App - Recipe*:	|
| :------------- | -----------: |
| ![Healthy Recipes app screenshot 1](img/home.PNG) | ![Healthy Recipes app screenshot 1](img/recipe.PNG) |


| *Screenshot of Java Skill Set 1 - Even or Odd*:      | *Screenshot of Java Skill Set 2 - Largest Number*:    | *Screenshot of Java Skill Set 3 - Arrays And Loops*:    |
| :------------- | :-----------: | -----------: |
|  ![EvenOrOdd.java Skillset](img/ss1.PNG) | ![LargestNumber.java Skillset](img/ss2.PNG) | ![ArraysAndLoops.java Skillset](img/ss3.PNG) |


