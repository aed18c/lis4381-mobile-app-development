<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Allyson Davis">

		<title>LIS4381 - RSS Feed</title>
</head>
<body>
	<?php
	$html = "";
	$publisher = "Science Daily News";
	$url = "http://feeds.sciencedaily.com/sciencedaily";

	$html .= '<h2 style="color: #800000; text-shadow: 2px 2px #e8d18c; font-size: 65px;">' . $publisher . '</h2>';
	$html .= '<a href="'.$url.'" style="color: #d9b034">'.$url.'</a>';

	$rss = simplexml_load_file($url);
	$count = 0;
	$html .= '<ul>';

	foreach($rss->channel->item as $item) {
		$count++;
		if ($count > 10) {
			break;
		}
		$html .= '<li><a href="'.htmlspecialchars($item->link).'" style="color: #b00202">'.htmlspecialchars($item->title).'</a><br />';
		$html .= htmlspecialchars($item->description).'<br />';
		$html .= htmlspecialchars($item->pubDate).'</li><br />';
	}
	$html .= '</ul>';

	print $html;
	?>
</body>
</html>
