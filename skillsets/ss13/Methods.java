//Allyson Davis
//LIS4381 ss13

import java.util.Scanner;
import  java.text.DecimalFormat;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Sphere Volume Program\n");
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches, and rounds to two decimal points.");
        System.out.println("Must use Java\'s *built-in* PI and pow() capabilities.");
        System.out.println("Program checks for non-integers and non-numericvalues.");
        System.out.println("Program continues to prompt for user entry until no longer requested, prompt accepts upper and lower case letters.\n");
    }//end getRequirements

public static void validateUserInput(){
    Scanner scnr = new Scanner(System.in);
    int diameter = 0;
    char choice = ' ';

    do {

        System.out.print("Please enter diameter in inches: ");

        while(!scnr.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter diameter in inches: ");
        }//end while

        diameter = scnr.nextInt();

        System.out.println("\nSphere volume: " + getVolume(diameter) + " liquid U.S. gallons\n");

        System.out.print("Do you wnat to calculate another sphere volume (y or n)? ");
        choice = scnr.next().charAt(0);

    } while(choice == 'y' || choice == 'Y');

    System.out.println("\nThank you for using our Sphere Volume Calculator!");

}//end validateUserInput

    public static String getVolume(int diameter) {
        //v = (4/3) * PI * r^3
        double radius = (diameter/2.0);

        double volume = ((4.0 / 3) * Math.PI * Math.pow(radius, 3) / 231);

        DecimalFormat f = new DecimalFormat("##.00");

        return String.format("%.2f", volume);
    }//end getVolume
}//end class