//Allyson Davis
//LIS 4381 - Skillset 5
import java.util.Scanner;
import java.util.Random;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Print minimum and maximum integer values.");
        System.out.println("Program prompt user to enter desired number of psuedorandom-generated integers (min 1).");
        System.out.println("Use the following loop structures: for, enhanced for, while, do...while.\n");

        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);

        System.out.println();
    }//end getRequirements

    public static int[] createArray() {
        Scanner scnr = new Scanner(System.in);
        int arraySize = 0;

        System.out.print("Enter desired number of psuedorandom integers (min 1): ");
        arraySize = scnr.nextInt();

        int yourArray[] = new int [arraySize];
        
        return yourArray;
    }//end createArray

    public static void generateNums(int [] myArray) {
        Random r = new Random();
        int i = 0;

        System.out.println("for loop:");
        for(i = 0; i < myArray.length; i++) {
            System.out.println(r.nextInt());
        }

        System.out.println("\nEnhanced for loop:");
        for(int n: myArray) {
            System.out.println(r.nextInt());
        }

        System.out.println("\nwhile loop:");
        i = 0;
        while(i < myArray.length) {
            System.out.println(r.nextInt());
            i++;
        }

        System.out.println("\ndo...while loop:");
        i = 0;
        do {
            System.out.println(r.nextInt());
            i++;
        }while(i < myArray.length);
    }//end generateNums
}