//Allyson Davis
//ss13 - Person.java 

public class Person {
    private String fname;
    private String lname;
    private int age;


    //requirements
    public static void getRequirements(){
        System.out.println("Developer: Allyson Davis");
    }

    //default constructor
    Person() {
        System.out.println("Inside person default constructor.\n");
        this.fname = "John";
        this.lname = "Doe";
        this.age = 21;
    }//end default constructor

    Person(String fname, String lname, int age) {
        System.out.println("\nInside person constructor with parameters.\n");
        this.fname = fname;
        this.lname = lname;
        this.age = age;
    }//end second constructor

    public void setFname(String fname) {
        this.fname = fname;
    }//end setFname

    public void setLname(String lname) {
        this.lname = lname;
    }//end setLname

    public void setAge(int age) {
        this.age = age;
    }//end setAge

    public String getLname() {
        return lname;
    }//end getLname

    public String getFname() {
        return fname;
    }//end getFname

    public int getAge() {
        return age;
    }//end getAge

    public void print() {
        System.out.println("Fname: " + getFname() + ", Lname: " + getLname() + ", Age: " + getAge());
    }
}//end class