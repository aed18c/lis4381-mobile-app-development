//Allyson Davis
//LIS 4381 - Skill set 6
import java.util.Scanner;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void method displays program requirements.");
        System.out.println("2) getUserInput(): Void method prompts for user input, \n\tthen calls two methods: myVoidMethod() and myValueReturningMethod().");
        System.out.println("3) myVoidMethod():\n\ta. Accepts two arguments: String and int.\n\tb. Prints user's first name and age.");
        System.out.println("4) myValueReturningMethod():\n\ta. Accepts two arguments: String and int.\n\tb. Returns String containing firstname and age.\n");
    }//end getRequirements

    public static void getUserInput() {
        String firstName = "";
        int userAge = 0;
        String myStr = "";
        Scanner scnr = new Scanner(System.in);

        //input
        System.out.print("Enter first name: ");
        firstName = scnr.next();

        System.out.print("Enter age: ");
        userAge = scnr.nextInt();

        System.out.println();

        System.out.print("Void method call: ");
        myVoidMethod(firstName, userAge);

        System.out.print("Value-returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }//end getUserInput

    public static void myVoidMethod(String name, int age) {
        System.out.println(name + " is " + age);
    }//end myVoidMethod

    public static String myValueReturningMethod(String name, int age) {
        return (name + " is " + age);
    }//end value returning method
}//end class