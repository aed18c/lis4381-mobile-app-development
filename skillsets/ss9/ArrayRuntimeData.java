//Allyson Davis
//LIS 4381 - skillset 9
//ArrayRuntimeData.java

import java.util.Scanner;

public class ArrayRuntimeData {
    static final Scanner scnr = new Scanner(System.in);

    public static void getRequirements() {
        System.out.println("Developer: Allyson Davis");
        System.out.println("1) Program create array size at run-time.");
        System.out.println("2) Program displays array size.");
        System.out.println("3) Program rounds sum and average of numbers to two decimal places.");
        System.out.println("4) Numbers *must* be float data type, not double.\n");
    }//end getRequirements

    public static int validateArraySize() {
        int arraySize = 0;

        System.out.print("Please enter array size: ");
        while(!scnr.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter array size: ");
        }//end while

        arraySize = scnr.nextInt();
        System.out.println();

        return arraySize;
    }//return validatArraySize

    public static void calculateNumbers(int arraySize) {
        float sum = 0.0f;
        float avg = 0.0F;

        System.out.print("Please enter " + arraySize + " numbers.\n");

        float numsArray[] = new float[arraySize];

        for(int i = 0; i < arraySize; i++) {
            System.out.print("Enter num " + (i + 1) + ": ");

            while(!scnr.hasNextFloat()) {
                System.out.println("Not valid number!\n");
                scnr.next();
                System.out.print("Please try again. Enter num " + (i + 1) + ": ");
            }//end while

            numsArray[i] = scnr.nextFloat();
            sum = sum + numsArray[i];
        }//end for

        avg = sum / arraySize;

        System.out.print("\nNumbers entered: ");
        for (int i = 0; i < numsArray.length; i++) {
            System.out.print(numsArray[i] + " ");
        }//end for

        printNumbers(sum, avg);
    }//end calculateNumbers

    public static void printNumbers(float sum, float avg) {
        System.out.println("\nSum: " + String.format("%.2f", sum));
        System.out.println("Average: " + String.format("%.2f", avg));
    }
}//end class