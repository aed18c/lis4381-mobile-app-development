//Allyson Davis
//ss13 - Employee.java 

public class Employee extends Person{
    private int ssn;
    private char gender;

    //no-par constructor (initializes to default values)
    Employee () {
        super();
        System.out.println("\nInside employee default constructor.\n");
        ssn = 123456789;
        gender = 'X';
    }

    Employee(String fname, String lname, int age, int ssn, char gender) {
        super(fname, lname, age);

        System.out.println("Inside employee constructor with parameters.\n");

        this.ssn = ssn;
        this.gender = gender;
    }

    public void setSsn(int ssn) {
        this.ssn = ssn;
    }//end setSsn

    public void setGender(char gender) {
        this.gender = gender;
    }//setGender

    public int getSsn() {
        return ssn;
    }//end getSsn

    public char getGender() {
        return gender;
    }//end getGender

    public void print() {
        System.out.println("Fname: " + getFname() + ", Lname: " + getLname() + ", Age: " + getAge() + ", SSN: " + getSsn() + ", Gender: " + getGender());
    }

}//end class