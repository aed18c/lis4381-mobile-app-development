//Allyson Davis
//LIS 4381 - Skill set 7
//Main.java

public class Main {
    public static void main(String args[]) {
        NestedStructures.getRequirements();

        int[] userArray = NestedStructures.createArray();

        NestedStructures.generatePseudoRandomNumbers(userArray);
    }
}//end class