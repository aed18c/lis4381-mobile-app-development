> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Allyson Davis
### Project #1 Requirements:

1. Backwards Engineer screenshots of example application
2. Create launcher icon image(s)
3. Add background color(s) to application
4. Add border around image and button
5. Add text shadow to button

#### README.md file should include the following items:

* Screenshot of First Business Card Application Interface
* Screenshot of Second Business Card Application Interface
* Screenshots of skillsets 7, 8, & 9


> 
> 
> 

#### Assignment Screenshots:


*Screenshot of My Business Card App*:

| *Screenshot of FIRST user interface*:      | *Screenshot of SECOND user interface*:    |
| :------------- | -----------: |
|  ![First User interface](img/screen1.PNG) | ![Second user interface](img/screen2.PNG)    |




*Screenshots of Skillsets 7, 8, & 9*:

| *Screenshot of Skillset 7*:      | *Screenshot of Skillset 8*:    | *Screenshot of Skillset 9*:    |
| :------------- | :-----------: | -----------: |
|  ![Skillset 7](img/ss7.PNG) | ![Skillset 8](img/ss8.PNG)    | ![Skillset 9](img/ss9.PNG)    |

