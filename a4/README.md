> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Allyson Davis
### Assignment #4 Requirements:

1. create local repos subdirectory
2. edit index.php meta tags, titles, navigation links and head tags
3. edit Assignment 4 index.php tp add form controls to match attributes of petstore entity
4. Add jQuert validation and regular expressions-- as per the entity attribute requirements

#### README.md file should include the following items:

* Screenshot of local lis4381 web app
* Screenshot of Failed form validation
* Screenshot of Passed form validation
* Link to local web app
* Screenshots of skillsets 10, 11, & 12


> 
> 
> 

#### Assignment Screenshots:

*LIS4381 Portal (Main Page)*:

![Local lis4381 home Screenshot](img/home.PNG)

*Screenshot of Form*:

| *Screenshot of FAILED validation*:      | *Screenshot of PASSED validation*:    |
| :------------- | -----------: |
|  ![screenshot of failed validation](img/invalid.PNG) | ![screenshot of passed validation](img/valid.PNG)    |


#### Assignment Links:

*Local LIS4381 web app:*
[local lis4381 link](http://localhost/repos/lis4381/)


*Screenshots of Skillsets 10, 11, & 12*:

| *Screenshot of Skillset 10*:      | *Screenshot of Skillset 11*:    |
| :------------- | | -----------: |
|  ![Skillset 10](img/ss10.PNG) | ![Skillset 11](img/ss11.PNG)    |


| *Screenshot of Skillset 12*:      | *Screenshot of Extra Skillset 12*:    | 
| :------------- | -----------: |
|  ![Skillset 12](img/ss12.PNG) | ![Skillset 12 extra cred](img/ss12_2.PNG)    |


