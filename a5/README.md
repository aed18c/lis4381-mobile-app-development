> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Allyson Davis
### Assignment #5 Requirements:

1. Create add_petstore.php 
2. Add server-side validation and regular expressions
3. Use regexp to only allow appropriate characters for each control
4. Link database

#### README.md file should include the following items:

* Screenshot of index.php showing database contents
* Screenshot testing server-side validaiton
* Screenshots of Skillsets 13, 14, and 15


> 
> 
> 

#### Assignment Screenshots:

*index.php*:

![A5 index Screenshot](img/data.PNG)

*add_petstore_process.php (includes error.php)*:

![A5 server-side validaiton Screenshot](img/validation.PNG)



#### Assignment Links:

*Local LIS4381 web app:*
[local lis4381 link](http://localhost/repos/lis4381/)


*Screenshots of Skillsets 13, 14, & 15*:

| *Screenshot of Skillset 13*:      | *Screenshot of Skillset 13 - Part 2*:    |
| :------------- | -----------: |
|  ![Skillset 13](img/ss13.PNG) | ![Skillset 13 pt2](img/ss13_2.PNG)    |


| *Screenshot of Skillset 14 (index.php)*:      | *Screenshot of Skillset 14 (process.php)*:    |
| :------------- | -----------: |
|  ![Skillset 14 index.php](img/ss14.PNG) | ![Skillset 14 process.php](img/ss14_1.PNG)    |

| *Screenshot of Skillset 14 (index.php)*:      | *Screenshot of Skillset 14 (process.php)*:    |
| :----------- | -----------: |
| ![Skillset 14 index.php](img/ss14_2.PNG)    | ![Skillset 14 process.php](img/ss14_3.PNG)    |


| *Screenshot of Skillset 15 (index.php)*:      | *Screenshot of Skillset 15 (process.php)*:    |
| :------------- | -----------: |
|  ![Skillset 15 index.php](img/ss15.PNG) | ![Skillset 15 process.php](img/ss15_2.PNG)    |


