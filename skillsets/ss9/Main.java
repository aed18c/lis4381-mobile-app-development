//Allyson Davis
//LIS 4381 - skillset 9
//Main.java

public class Main {
    public static void main(String args[]) {
        ArrayRuntimeData.getRequirements();
        
        int arraySize = 0;
        arraySize = ArrayRuntimeData.validateArraySize();

        ArrayRuntimeData.calculateNumbers(arraySize);
    }//end main
}//end class