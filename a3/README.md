> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Allyson Davis
### Assignment #3 Requirements:

1. Create ERD with MySQL Workbench
2. Forward engineer database with 10 records in each table
3. Create event app with launcher icon
4. Add colors to activity controls
5. Add border around image and button
6. Add text shadow (button)

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second use interface
* Links to a3.mwb and a3.sql
* Screenshots of skillsets 4, 5, & 6


> 
> 
> 

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/erd.PNG)

*Screenshot of My Event App*:

| *Screenshot of FIRST user interface*:      | *Screenshot of SECOND user interface*:    | *Another screenshot of SECOND user interface*:    |
| :------------- | :-----------: | -----------: |
|  ![First User interface](img/screen1.PNG) | ![Second user interface](img/screen2.PNG)    | ![Second user interface](img/screen3.PNG)    |


#### Assignment Links:

*A3.MWB:*
[a3.mwb link](docs/a3.mwb)

*A3.SQL*
[a3.sql link](docs/a3.sql)


*Screenshots of Skillsets 4, 5, & 6*:

| *Screenshot of Skillset 4*:      | *Screenshot of Skillset 5*:    | *Screenshot of Skillset 6*:    |
| :------------- | :-----------: | -----------: |
|  ![Skillset 4](img/ss4.PNG) | ![Skillset 5](img/ss5.PNG)    | ![Skillset 6](img/ss6.PNG)    |

