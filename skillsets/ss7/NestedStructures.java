//Allyson Davis
//LIS 4381 - Skill set 7
//NestedStructures.java

import java.util.Scanner;
import java.util.Random;

public class NestedStructures {
    public static void getRequirements() {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Print minimum and maximum integer values.");
        System.out.println("Program prompts user to enter desired number of psuedorandom-generated integrs (min 1).");
        System.out.println("Program validates user input for integers greater than 0.");
        System.out.println("Use following loop structues: for, enhanced for, while, do...while.\n");

        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);

        System.out.println();
    }//end getReuirements

    public static int[] createArray() {
        Scanner scnr = new Scanner(System.in);
        int arraySize = 0;

        System.out.print("Enter desired number of psuedo-random integers (min 1): ");

        while(!scnr.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter valid integer (min 1): ");
        }//end while

        arraySize = scnr.nextInt();

        while(arraySize < 1) {
            System.out.print("\nNumber must be greater than 0. Please enter integer greater than 0: ");

            while(!scnr.hasNextInt()) {
                System.out.println("Not valid integer!\n");
                scnr.next();
                System.out.print("Please try again. Enter integer value greater than 0: ");
            }//end while
            arraySize = scnr.nextInt();
        }//end while arraySize<1
        int yourArray[] = new int[arraySize];
        return yourArray;
    }//end createArray

    public static void generatePseudoRandomNumbers(int [] myArray) {
        Random r = new Random();
        int i = 0;

        System.out.println("for loop:");
        for (i = 0; i < myArray.length; i++) {
            System.out.println(r.nextInt());
        }//end for

        System.out.println("\nEnhanced for loop:");
        for(int n: myArray) {
            System.out.println(r.nextInt());
        }//end enhanced for

        System.out.println("\nwhile loop:");
        i = 0;
        while(i < myArray.length) {
            System.out.println(r.nextInt());
            i++;
        }//end while

        i = 0;
        System.out.println("\n do...while loop:");
        do {
            System.out.println(r.nextInt());
            i++;
        } while (i < myArray.length);
    }//end generateNums
}//end class