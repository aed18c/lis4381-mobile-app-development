<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Allyson Davis">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Skill Sets</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<p>
					<h4>Skill Set 1</h4>
					<a href="ss1/EvenOrOdd.java">EvenOrOdd.java</a>
				</p>
				<img src="ss1/ss1.PNG" class="img-responsive center-block" alt="running ss1">

				<p>
					<h4>Skill Set 2</h4>
					<a href="ss2/LargestNumber.java">LargestNumber.java</a>
				</p>
				<img src="ss2/ss2.PNG" class="img-responsive center-block" alt="running ss2">

				<p>
					<h4>Skill Set 3</h4>
					<a href="ss3/ArraysAndLoops.java">ArraysAndLoops.java</a>
				</p>
				<img src="ss3/ss3.PNG" class="img-responsive center-block" alt="running ss3">

				<p>
					<h4>Skill Set 4</h4>
					<a href="ss4/DecisionStructures.java">DecisionStructures.java</a><br>
					<a href="ss4/Methods.java">Methods.java</a>
				</p>
				<img src="ss4/ss4.PNG" class="img-responsive center-block" alt="running ss4">

				<p>
					<h4>Skill Set 5</h4>
					<a href="ss5/RandomArray.java">RandomArray.java</a><br>
					<a href="ss5/Methods.java">Methods.java</a>
				</p>
				<img src="ss5/ss5.PNG" class="img-responsive center-block" alt="running ss5">
				
				<p>
					<h4>Skill Set 6</h4>
					<a href="ss6/Main.java">Main.java</a><br>
					<a href="ss6/Methods.java">Methods.java</a>
				</p>
				<img src="ss6/ss6.PNG" class="img-responsive center-block" alt="running ss6">

				<p>
					<h4>Skill Set 7</h4>
					<a href="ss7/Main.java">Main.java</a><br>
					<a href="ss7/NestedStructures.java">NestedStructures.java</a>
				</p>
				<img src="ss7/ss7.PNG" class="img-responsive center-block" alt="running ss7">

				<p>
					<h4>Skill Set 8</h4>
					<a href="ss8/Main.java">Main.java</a><br>
					<a href="ss8/LargestOfThreeNumbers.java">LargestOfThreeNumbers.java</a>
				</p>
				<img src="ss8/ss8.PNG" class="img-responsive center-block" alt="running ss8">

				<p>
					<h4>Skill Set 9</h4>
					<a href="ss9/Main.java">Main.java</a><br>
					<a href="ss9/ArrayRuntimeData.java">ArrayRuntimeData.java</a>
				</p>
				<img src="ss9/ss9.PNG" class="img-responsive center-block" alt="running ss9">

				<p>
					<h4>Skill Set 10</h4>
					<a href="ss10/Main.java">Main.java</a><br>
					<a href="ss10/Methods.java">Methods.java</a>
				</p>
				<img src="ss10/ss10.PNG" class="img-responsive center-block" alt="running ss10">

				<p>
					<h4>Skill Set 11</h4>
					<a href="ss11/Main.java">Main.java</a><br>
					<a href="ss11/Methods.java">Methods.java</a>
				</p>
				<img src="ss11/ss11.PNG" class="img-responsive center-block" alt="running ss11">

				<p>
					<h4>Skill Set 12</h4>
					<a href="ss12/Main.java">Main.java</a><br>
					<a href="ss12/Methods.java">Methods.java</a>
				</p>
				<img src="ss12/ss12.PNG" class="img-responsive center-block" alt="running ss12">

				<p>
					<h4>Skill Set 12 - Extra</h4>
					<a href="ss12_2/PersonDemo.java">PersonDemo.java</a><br>
					<a href="ss12_2/Person.java">Person.java</a>
				</p>
				<img src="ss12_2/ss12_2.PNG" class="img-responsive center-block" alt="running ss12_2">

				<p>
					<h4>Skill Set 13</h4>
					<a href="ss13/Main.java">Main.java</a><br>
					<a href="ss13/Methods.java">Methods.java</a>
				</p>
				<img src="ss13/ss13.PNG" class="img-responsive center-block" alt="running ss13">

				<p>
					<h4>Skill Set 13 - Extra</h4>
					<a href="ss13_2/Person.java">Peron.java</a><br>
					<a href="ss13_2/Employee.java">Employee.java</a><br>
					<a href="ss13_2/EmployeeDemo.java">EmployeeDemo.java</a>
				</p>
				<img src="ss13_2/ss13_2.PNG" class="img-responsive center-block" alt="running ss13 extra">

				<p>
					<h4>Skill Set 14</h4>
					<a href="ss14/index.php">index.php</a><br>
					<a href="ss14/process.php">process.php</a>
				</p>
				<img src="ss14/img/ss14.PNG" class="img-responsive center-block" alt="running ss14 index">
				<img src="ss14/img/ss14_1.PNG" class="img-responsive center-block" alt="running ss14 process">

				<p>
					<h4>Skill Set 15</h4>
					<a href="ss15/index.php">index.php</a><br>
					<a href="ss15/process.php">process.php</a>
				</p>
				<img src="ss15/img/ss15.PNG" class="img-responsive center-block" alt="running ss15 index">
				<img src="ss15/img/ss15_2.PNG" class="img-responsive center-block" alt="running ss15 process">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
