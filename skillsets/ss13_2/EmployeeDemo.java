//Allyson Davis
//ss13 - EmployeeDemo.java 

import java.util.Scanner;

public class EmployeeDemo {
    public static void main(String args[]) {
        Person.getRequirements();
        Scanner scnr = new Scanner(System.in);

        System.out.println("/////Below are the base class default constructor values://///\n");
        Person person1 = new Person();

        System.out.println("Fname = " + person1.getFname());
        System.out.println("Lname = " + person1.getLname());
        System.out.println("Age = " + person1.getAge());

        System.out.println("\n/////Below are user-entered values://///\n");
        System.out.print("Fname: ");
        String fname = scnr.nextLine();
        System.out.print("Lname: ");
        String lname = scnr.nextLine();
        System.out.print("Age: ");
        int age = scnr.nextInt();

        Person person2 = new Person(fname, lname, age);
        System.out.println("Fname = " + person2.getFname());
        System.out.println("Lname = " + person2.getLname());
        System.out.println("Age = " + person2.getAge());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values/////");
        person1.setFname("Bob");
        person1.setLname("Wilson");
        person1.setAge(42);
        person1.print();

        System.out.println("\n/////Below are derived class default constructor values://///");
        Employee emp1 = new Employee();

        System.out.println("Fname = " + emp1.getFname());
        System.out.println("Lname = " + emp1.getLname());
        System.out.println("Age = " + emp1.getAge());
        System.out.println("SSN = " + emp1.getSsn());
        System.out.println("Gender = " + emp1.getGender());

        System.out.println("\nOr...\n");
        emp1.print();

        scnr.nextLine();

        System.out.println("\n/////Below are dervied class user-entered values://///\n");
        System.out.print("Fname: ");
        String first = scnr.nextLine();
        System.out.print("Lname: ");
        String last = scnr.nextLine();
        System.out.print("Age: ");
        int age2 = scnr.nextInt();
        System.out.print("SSN: ");
        int ssn = scnr.nextInt();
        System.out.print("Gender: ");
        char gender = scnr.next().charAt(0);

        Employee emp2 = new Employee(first, last, age2, ssn, gender);

        System.out.println("Fname = " + emp2.getFname());
        System.out.println("Lname = " + emp2.getLname());
        System.out.println("Age = " + emp2.getAge());
        System.out.println("SSN = " + emp2.getSsn());
        System.out.println("Gender = " + emp2.getGender());

        System.out.println("\nOr...\n");
        emp2.print();

    }//end main
}//end class