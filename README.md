
# LIS4381 Mobile Web App Development

## Allyson Davis

### Assignment Requirements:

*Assignment Links :*

1. [A1 README.md](a1/README.md "My A1 README.md file")

      - Install AMPPS
      - Install JDK
      - Install Android Studio and create My First App  
      - Create Bitbucket repo
      - Complete Bitbucket Tutorials (bitbucketstationlocations and myteamquotes)
      - Provide git command descripteds
      - Provide screenshots of installations

2. [A2 README.md](a2/README.md "My A2 README.md file")
   
      - Create Healthy Recipes Android app
      - Provide screenshots of completed app
      - Provide screenshots of completed Java skill sets

3. [A3 README.md](a3/README.md "My A3 README.md file")

      - Create ERD with MySQL Workbench
      - Add 10 records to each table
      - Provide Screenshots of ERD
      - Create MyEvent Application
      - Provide screenshots of first and second user interface
      - Provid links to a3.mwb and a3.sql

4. [A4 README.md](a4/README.md "My A4 README.md file")

      - create local repos subdirectory
      - edit index.php meta tags, titles, navigation links and head tags
      - edit Assignment 4 index.php tp add form controls to match attributes of petstore entity
      - Add jQuert validation and regular expressions-- as per the entity attribute requirements

5. [A5 README.md](a5/README.md "My A5 README.md file")

     - Create add_petstore.php 
     - Add server-side validation and regular expressions
     - Use regexp to only allow appropriate characters for each control
     - Link database

### Project Requirements:

*Project Links :*

1. [P1 README.md](p1/README.md "My P1 README.md file")
 
    - Backwards Engineer screenshots of example application
    - Create launcher icon image(s)
    - Add background color(s) to application
    - Add border around image and button
    - Add text shadow to button

2. [P2 README.md](p2/README.md "My P2 README.md file")

    - Add server-side validation and regular expressions
    - Add edit_petstore.php and edit_petstore_process.php
    - Add delete_petstore.php
    - Create a simple rss feed


