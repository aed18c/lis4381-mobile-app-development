//Allyson Davis
//LIS 4381 - Skillset 8
//LargestOfThreeNumbers.java

import java.util.Scanner;

public class LargestOfThreeNumbers {
    public static void getRequirements() {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Program evaluates largest of three integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.\n");
    }//end getRequirements

    public static void validateUserInput(){
        Scanner scnr = new Scanner(System.in);
        int num1 = 0, num2 = 0, num3 = 0;
        
        System.out.print("Please enter first number: ");
        while(!scnr.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter first number: ");
        }//end while
        num1 = scnr.nextInt();

        System.out.print("\nPlease enter second number: ");
        while(!scnr.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter second number: ");
        }//end while
        num2 = scnr.nextInt();

        System.out.print("\nPlease enter third number: ");
        while(!scnr.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter third number: ");
        }//end while
        num3 = scnr.nextInt();

        getLargestNumber(num1, num2, num3);
    }//end validateUserInput

    public static void getLargestNumber(int num1, int num2, int num3) {
        if (num1 > num2 && num1 > num3) {
            System.out.println("\nFirst number is largest.");
        }
        else if (num2 > num1 && num2 > num3) {
            System.out.println("\nSecond number is largest.");
        }
        else if (num3 > num1 && num3 > num2) {
            System.out.println("\nThird number is largest.");
        }
        else {
            System.out.println("\nIntegers are equal.");
        }
    }//end getLargestNumber
}//end of class